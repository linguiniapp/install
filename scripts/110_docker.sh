#!/bin/bash

# Agrega la clave GPG de Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
    | sudo apt-key add -

# Agrega el repositorio de Docker para Ubuntu
sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable"

# Actualiza la lista de aplicaciones e instala Docker
sudo apt-get -qqy update
sudo apt-get -qqy install docker-ce
