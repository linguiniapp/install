#!/bin/bash

# Agrega los repositorios de nodejs
echo "Agregando repositorios de NodeJS"
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

# Instala NodeJS y NPM
echo "Instalando NodeJS y NPM"
sudo apt-get -qy install nodejs
