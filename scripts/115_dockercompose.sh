#!/bin/bash

# Descarga docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.22.0-rc1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

sudo docker-compose

echo "Agregando usuario a grupo docker"
docker_user=$(whoami)
sudo usermod -a -G docker ${docker_user}
