#!/bin/bash

# Informa sobre ubicacion de instalación
echo "Instala los entornos de desarrollo para LinguiniApp"
echo 'Crea una carpeta "linguiniapp" en la ruta indicada'
echo -n "Ruta de instalacion del entorno [./] :"
read ruta
if [ -z "$ruta" ]; then
    ruta="./linguiniapp"
else
    ruta=$ruta"/linguiniapp"
fi
mkdir -p ${ruta}
cd ${ruta}

# Actualiza el sistema
echo "Actualizando el sistema"
sudo apt-get -qqy update
sudo apt-get -qqy upgrade

# Instala programas necesarios
echo "Instalando prerrequisitos"
sudo apt-get -qqy install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# Instala Git
echo "Instalando Git"
sudo apt-get -qqy install git

# Configura nombre y correo de usuario
echo "Informacion de usuario para Git:"
echo -n "Nombre completo: "
read git_user
git config --global user.name "$git_user"
echo -n "Correo electronico: "
read git_email
git config --global user.email "$git_email"

# Configura el modo de push (la otra opcion es matching, dejenlo asi)
git_push="simple"
git config --global push.default $git_push

# Inicia sesion en GitLab
echo "Inicio de sesion en GitLab:"
echo -n "Usuario: "
read gitlab_user
echo -n "Contraseña: "
read -s gitlab_password
echo ""

echo -n "Nombre para la clave SSH (identifica la PC): "
read nombre_clave

gitlab_host="https://gitlab.com"

# Solicita la pagina de login para obtener cookies y token
echo "Iniciando sesion"
body_header=$(curl -c cookies.txt -i "${gitlab_host}/users/sign_in" -s)

# Busca el token en la pagina
csrf_token=$(echo $body_header | perl -ne 'print "$1\n" if /new_user.*?authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

# Envia credenciales para iniciar sesion usando las cookies y el token recibidos
curl -b cookies.txt -c cookies.txt -i "${gitlab_host}/users/sign_in" \
    --data "user[login]=${gitlab_user}&user[password]=${gitlab_password}" \
    --data-urlencode "authenticity_token=${csrf_token}"

# Solicita la pagina de tokens de acceso personal de GitLab para obtener un token
echo "Obteniendo un token de acceso personal"
body_header=$(curl -H 'user-agent: curl' -b cookies.txt -i "${gitlab_host}/profile/personal_access_tokens" -s)

# Busca el token en la respuesta
csrf_token=$(echo $body_header | perl -ne 'print "$1\n" if /authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

# Envia una solicitud para generar un token de acceso personal
exp_date=$(date -u -I -d '+1 day')
body_header=$(curl -L -b cookies.txt "${gitlab_host}/profile/personal_access_tokens" \
    --data-urlencode "authenticity_token=${csrf_token}" \
    --data 'personal_access_token[name]=linguini-install&personal_access_token[expires_at]='"${exp_date}"'&personal_access_token[scopes][]=api')

# Busca el token de acceso personal en la respuesta
personal_access_token=$(echo $body_header | perl -ne 'print "$1\n" if /created-personal-access-token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

# Busca o genera un par RSA para configurar SSH en GitLab
echo "Generando clave RSA para SSH"
echo | ssh-keygen -q -N "" >/dev/null

# Agrega la clave SSH al usuario de GitLab
echo "Agregando clave para SSH a GitLab"
pubkey=$(cat ~/.ssh/id_rsa.pub)
curl -i -H "Private-Token: ${personal_access_token}" -H "Content-Type:application/json" \
    -X POST --data '{ "key": "'"${pubkey}"'", "title": "'"${nombre_clave}"'"}' \
    https://gitlab.com/api/v4/user/keys

# Elimina las cookies utilizadas
rm cookies.txt

# Agrega GitLab a los host conocidos
touch ~/.ssh/known_hosts
ssh-keyscan gitlab.com 2>&1 | sort -u - ~/.ssh/known_hosts > ~/.ssh/tmp_hosts
mv ~/.ssh/tmp_hosts ~/.ssh/known_hosts

# Descarga el resto de los scripts
echo "Descargando scripts adicionales"
git clone git@gitlab.com:linguiniapp/install.git .

# Clona los respositorios del proyecto
echo "Clonando repositorios del proyecto"
git clone git@gitlab.com:linguiniapp/backend.git
git clone git@gitlab.com:linguiniapp/frontend-web.git

# Ejecuta scripts de configuracion adicional
echo "Ejecutando scripts"
chmod +x ./scripts/*
for filename in ./scripts/*.sh; do
    $filename
done

# Restaura permisos erroneos en configuraciones de usuario
echo "Reparando permisos"
sudo chown -R $USER:$(id -gn $USER) ~/.config

# Construye los entornos de desarrollo
echo "Construyendo entornos"
cd backend
./build.sh
cd ..
cd frontend-web
./build.sh
cd ..

# Elimina script original
rm ../linguini-install.sh

echo -e "\n########################################################\n"
echo "Backend:"
echo "        Activar el entorno con 'linguiniapp start'"
echo "        Desactivar el entorno con 'linguiniapp stop'"
echo "        Ejecutar localmente con 'linguiniapp run'"
echo "\nPostgres:"
echo "        Usuario: postgres"
echo "        Contraseña: postgres"
echo "\nPgAdmin:"
echo "        Usuario: dbadmin@linguiniapp.com"
echo "        Contraseña: asd"
echo -e "\n########################################################\n"
echo "¡Todo listo!"
